1. # OnlineCourseSelectionSystem

   #### 介绍

   前端使用semanticUI渲染及Ajax动态填写，后端采用基于springboot，涉及的技术栈有mybatis，spring，springMVC，spring security，log4j，swaggerUI等。IDE工具: IDEA
   适合大学生课设实训等作业。

   #### 软件架构

   软件架构说明


   #### 安装教程

   1.  拉取本仓库后, 静待对应jar包下载，或自行修改jdk配置及maven配置。
   2.  根据yml文件中的redis,自行下载redis并配对好端口
   3.  先运行redis后,再启动项目.
   4.  访问localhost:8080即可

   #### 另外说明

   1. 本项目swagger在线文档有两种显示网站

      localhost:8080/doc.html

      localhost:8080/swagger-ui.html

   #### 参与贡献

   1.  Fork 本仓库并star本仓库
  
