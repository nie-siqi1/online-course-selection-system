package cn.gdpu.controller;

import cn.gdpu.bean.SelectableCourse;
import cn.gdpu.mapper.SelectableCourseDAO;
import cn.gdpu.service.UserCourseService;
import cn.gdpu.util.Msg;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName UserCourseController
 * @Author ttaurus
 * @Date Create in 2020/3/9 15:08
 */
@RestController
@RequestMapping("usercourse")
@Api
public class UserCourseController{

    @Autowired
    UserCourseService userCourseService;
    @Autowired
    SelectableCourseDAO selectableCourseDAO;

    /**
     * 选课
     * @param courseId
     * @param username
     * @return
     */
    @PostMapping("choose")
    @PreAuthorize("hasAuthority('student')")
    public Object chooseCourse(@RequestParam("courseId") Integer courseId ,
                               @RequestParam("username") String username){
        Map<String,Object> map = new HashMap<>();
        try{
            return userCourseService.chooseCourse(courseId , username);
        }catch(Exception e){
            if(e instanceof DataIntegrityViolationException){
                map.put("msg","该课程已经被抢完啦。");
            }else{
                map.put("msg","出现其他异常,选课失败!");
            }
            map.put("flag",false);
            return JSON.toJSON(map);
        }
    }

    /**
     * 退课
     * @param courseId
     * @param username
     * @return
     */
    @PostMapping("cancel")
    @PreAuthorize("hasAuthority('student')")
    public Object cancelCourse(@RequestParam("courseId") Integer courseId ,
                               @RequestParam("username") String username){
        return userCourseService.cancelCourse(courseId,username);
    }

    /**
     * 获取学生所选全部课程
     * @param page
     * @param limit
     * @param username
     * @return
     */
    @PostMapping("studentInfo")
    @PreAuthorize("hasAuthority('admin') or hasAuthority('student')")
    public Object studentInfo(@RequestParam(value = "page", defaultValue = "1") int page ,
                              @RequestParam(value = "limit", defaultValue = "10") int limit ,
                              @RequestParam("username")String username){
        try{
            Map<String,Object> map = new HashMap<>();
            PageHelper.startPage(page , limit);
            List<SelectableCourse> list = selectableCourseDAO.selectByUser(username);
            if(list == null){
                return Msg.fail();
            }
            //System.out.println("=="+username+"==");
            PageInfo<SelectableCourse> pageInfo = new PageInfo<>(list);
            map.put("totalPage" , pageInfo.getPages());  //总页数
            map.put("totalCount" , pageInfo.getTotal());  //总条数
            map.put("currentPage" , page);  //当前页数。
            map.put("data" , pageInfo.getList()); //获得的数据量
            map.put("tCase",username);
            return JSON.toJSON(map);
        }catch(Exception e){
            e.printStackTrace();
            return Msg.fail();
        }
    }
    
    //测试。
    @PostMapping("cc")
    public Object cc(){
        Map<String,Object> map = new HashMap<>();
        try{
            selectableCourseDAO.updateMinCourseStock(1);
            return true;
        }catch(Exception e){
            if(e instanceof DataIntegrityViolationException){
                map.put("msg","该课程已经被抢完啦。");
            }else{
                map.put("msg","出现其他异常,选课失败!");
            }
            map.put("flag",false);
            return JSON.toJSON(map);
        }
    }
}

