package cn.gdpu.mapper;

import cn.gdpu.bean.SelectableCourse;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @ClassName SelectableCourseDAO
 * @Author ttaurus
 * @Date Create in 2020/3/4 9:04
 */
@Mapper
@Repository
public interface SelectableCourseDAO{

    void insert(SelectableCourse selectableCourse);
    
    List<SelectableCourse> selectCourses();
    
    List<SelectableCourse> selectCoursesStocks(String username);
    
    //根据课程类别搜索
    List<SelectableCourse> selectCoursesByType(String type,String username);
    
    //根据课程所属学院名称搜索
    List<SelectableCourse> selectCoursesByCollege(String name,String username);
    
    //根据剩余人数搜索课程
    List<SelectableCourse> selectCourseByMemberCount(Integer start,Integer end,String username);
    
    //增加库存
    void updateAddCourseStock(Integer courseId);

    void updateMinCourseStock(Integer courseId);
    
    //获得库存
    Integer getStockByCourseId(Integer courseId);
    
    //根据名称模糊搜索
    List<SelectableCourse> selectByCourseName(String name,String username);
    
    //搜索出该学生已选的课程
    List<SelectableCourse> selectByUser(String username);
    
    //拿到所有已被选择的课程
    List<SelectableCourse> selectedCourses();
    
    //隐藏课程
    void hideBatch(Integer courseId);
    
    //添加课程
    void addCourse(String courseName,Integer collegeId,String courseType,String teacher,Integer score,Integer stock,
                   String address,String description);
}
